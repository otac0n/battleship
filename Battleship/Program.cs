﻿namespace Battleship
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var allOpponents = FindOpponent("ProbaBallistic", new Version(3, 0));

            foreach (var opponenet in allOpponents)
            {
                Console.Write("{0} {1},", opponenet.Name, opponenet.Version);
                var metrics = new PlayerMetrics(opponenet, new Size(10, 10), new[] { 2, 3, 3, 4, 5 }).CalculateMetrics();
                Console.WriteLine(string.Join(",", metrics.Select(m => m.ToString()).ToArray()));
                File.AppendAllText("runs.txt", opponenet.Name + " " + opponenet.Version + string.Join(",", metrics.Select(m => m.ToString()).ToArray()));
            }

            Console.ReadKey(true);
        }

        private static IBattleshipOpponent[] FindOpponent(string name, Version version = null)
        {
            return (from op in FindAllOpponents()
                    where op.Name.Equals(name, StringComparison.OrdinalIgnoreCase)
                    where version == null || op.Version == version
                    select op).ToArray();
        }

        private static IBattleshipOpponent[] FindAllOpponents()
        {
            var allOpponents = (from t in Assembly.GetExecutingAssembly().GetTypes()
                                where t.GetInterfaces().Any(i => i == typeof(IBattleshipOpponent))
                                let c = t.GetConstructor(new Type[0])
                                where c != null
                                select (IBattleshipOpponent)c.Invoke(null)).ToArray();
            return allOpponents;
        }
    }
}
