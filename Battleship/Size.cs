﻿namespace Battleship
{
    public struct Size
    {
        private readonly int width;
        private readonly int height;

        public Size(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public int Width
        {
            get { return this.width; }
        }

        public int Height
        {
            get { return this.height; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Size))
            {
                return false;
            }

            Size size = (Size)obj;
            return (size.width == this.width) &&
                   (size.height == this.height);
        }

        public override int GetHashCode()
        {
            return this.width ^ this.height;
        }

        public static bool operator ==(Size left, Size right)
        {
            return (left.width == right.width) &&
                   (left.height == right.height);
        }

        public static bool operator !=(Size left, Size right)
        {
            return (left.width != right.width) ||
                   (left.height != right.height);
        }
    }
}
