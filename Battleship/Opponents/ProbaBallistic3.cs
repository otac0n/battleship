﻿namespace Battleship.Opponents
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public class ProbaBallistic3 : IBattleshipOpponent
    {
        private readonly Random rand = new Random();
        private Size gameSize;
        private int[] shipSizes;
        private int[] shipSizeSum;
        private List<Ship>[] shipStates;
        private HashSet<Point> shots = new HashSet<Point>();
        private HashSet<Point> hits = new HashSet<Point>();
        private HashSet<Point> misses = new HashSet<Point>();

        public string Name
        {
            get { return "ProbaBallistic"; }
        }

        public Version Version
        {
            get { return new Version(3, 0); }
        }

        public void NewMatch(string opponent)
        {
        }

        public void NewGame(Size size, TimeSpan timeSpan, int[] shipSizes)
        {
            this.gameSize = size;
            this.shipSizes = shipSizes.OrderByDescending(s => s).ToArray();
            this.shipSizeSum = Enumerable.Range(0, this.shipSizes.Length + 1).Select(s => this.shipSizes.Take(s).Sum()).Reverse().ToArray();
            this.shots.Clear();
            this.misses.Clear();
            this.hits.Clear();

            GenShipStates();
        }

        private void GenShipStates()
        {
            var shipStates = new List<Ship>[this.shipSizes.Length];
            for (int s = 0; s < shipSizes.Length; s++)
            {
                shipStates[s] = GenShipStates(s);
            }

            this.shipStates = shipStates;
        }

        private List<Ship> GenShipStates(int s)
        {
            var size = this.shipSizes[s];
            var ships = new List<Ship>();

            for (int o = 0; o < 2; o++)
            {
                var orientation = (ShipOrientation)o;

                var width = this.gameSize.Width -
                    (orientation == ShipOrientation.Horizontal ? size - 1 : 0);

                var height = this.gameSize.Height -
                    (orientation == ShipOrientation.Vertical ? size - 1 : 0);

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        ships.Add(
                            new Ship(size, new Point(x, y), orientation));
                    }
                }
            }

            return ships;
        }

        private bool GenSingleState(int s, Ship[] ships, int remainingHits, int[,] probs)
        {
            if (remainingHits > this.shipSizeSum[s])
            {
                return false;
            }

            if (s >= ships.Length)
            {
                foreach (var ship in ships)
                {
                    foreach (var point in ship.GetAllLocations())
                    {
                        if (probs[point.X, point.Y] != -1)
                        {
                            probs[point.X, point.Y]++;
                        }
                    }
                }

                return true;
            }
            else
            {
                var index = rand.Next(this.shipStates[s].Count);
                var ship = this.shipStates[s][index];

                ships[s] = ship;

                for (int i = 0; i < s; i++)
                {
                    if (ships[i].ConflictsWith(ship))
                    {
                        return false;
                    }
                }

                return GenSingleState(s + 1, ships, remainingHits - this.hits.Count(h => ship.IsAt(h)), probs);
            }
        }

        public IList<Ship> PlaceShips()
        {
            var ships = new Ship[this.shipSizes.Length];

            for (int i = 0; i < ships.Length; i++)
            {
                ships[i] = new Ship(
                    this.shipSizes[i],
                    new Point(
                        rand.Next(this.gameSize.Width),
                        rand.Next(this.gameSize.Height)),
                    (ShipOrientation)this.rand.Next(2));
            }

            return ships;
        }

        public Point GetShot()
        {
            var probs = GetProbabilities();
            Point point = FindMostLikelyPoint(probs);
            this.shots.Add(point);
            return point;
        }

        private void ApplyHit(Point hit)
        {
        }

        private void ApplyMiss(Point miss)
        {
            foreach (var state in this.shipStates)
            {
                for (var i = state.Count - 1; i >= 0; i--)
                {
                    if (state[i].IsAt(miss))
                    {
                        state.RemoveAt(i);
                    }
                }
            }
        }

        private Point FindMostLikelyPoint(int[,] probs)
        {
            int max = -1;
            var point = new Point(-1, -1);

            for (int y = 0; y < this.gameSize.Height; y++)
            {
                for (int x = 0; x < this.gameSize.Width; x++)
                {
                    if (probs[x, y] > max)
                    {
                        max = probs[x, y];
                        point = new Point(x, y);
                    }
                }
            }

            return point;
        }

        private int[,] GetProbabilities()
        {
            var board = new int[this.gameSize.Width, this.gameSize.Height];
            foreach (var shot in this.shots)
            {
                board[shot.X, shot.Y] = -1;
            }

            int successes = 0;
            for (int i = 0; successes < 100; i++)
            {
                if (this.GenSingleState(0, new Ship[this.shipSizes.Length], this.hits.Count, board))
                {
                    successes++;
                }
            }

            RenderBoard(board);

            return board;
        }

        [Conditional("DEBUG")]
        private void RenderBoard(int[,] board)
        {
            Console.WriteLine();

            for (int y = 0; y < this.gameSize.Height; y++)
            {
                for (int x = 0; x < this.gameSize.Width; x++)
                {
                    if (this.hits.Contains(new Point(x, y)))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }

                    Console.Write("{0,5} ", board[x, y]);
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Console.WriteLine();
            }

            Console.ReadKey();
        }

        public void OpponentShot(Point shot)
        {
        }

        public void ShotHit(Point shot, bool sunk)
        {
#if DEBUG
            Console.WriteLine("Shot hit: {0},{1}", shot.X, shot.Y);
#endif
            this.hits.Add(shot);
            ApplyHit(shot);
        }

        public void ShotMiss(Point shot)
        {
#if DEBUG
            Console.WriteLine("Shot missed: {0},{1}", shot.X, shot.Y);
#endif
            this.misses.Add(shot);
            ApplyMiss(shot);
        }

        public void GameWon()
        {
        }

        public void GameLost()
        {
        }

        public void MatchOver()
        {
        }
    }
}
