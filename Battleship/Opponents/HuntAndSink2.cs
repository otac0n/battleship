﻿namespace Battleship.Opponents
{
    using System;
    using System.Collections.Generic;

    public class HuntAndSink2Opponent : IBattleshipOpponent
    {
        private readonly Random rand;
        private Size boardSize;
        private int[] shipLengths;

        private readonly HashSet<Point> shots;
        private readonly Queue<Point> priority;

        public HuntAndSink2Opponent()
        {
            this.rand = new Random();
            this.shots = new HashSet<Point>();
            this.priority = new Queue<Point>();
        }

        public string Name
        {
            get { return "Hunt and Sink"; }
        }

        public Version Version
        {
            get { return new Version(2, 0); }
        }

        public void NewMatch(string opponent)
        {
        }

        public void NewGame(Size size, TimeSpan timeSpan, int[] shipLengths)
        {
            this.boardSize = size;
            this.shipLengths = shipLengths;
            this.shots.Clear();
        }

        public IList<Ship> PlaceShips()
        {
            var ships = new Ship[this.shipLengths.Length];

            for (int i = 0; i < ships.Length; i++)
            {
                ships[i] = new Ship(
                    this.shipLengths[i],
                    new Point(
                        this.rand.Next(this.boardSize.Width),
                        this.rand.Next(this.boardSize.Height)),
                    (ShipOrientation)this.rand.Next(2));
            }

            return ships;
        }

        public Point GetShot()
        {
            Point shot;

            if (this.priority.Count > 0)
            {
                shot = this.priority.Dequeue();
                this.shots.Add(shot);
                return shot;
            }

            do
            {
                shot = new Point(
                    rand.Next(this.boardSize.Width),
                    rand.Next(this.boardSize.Height));
            }
            while (((shot.X + shot.Y) % 2 != 0) || !this.shots.Add(shot));

            return shot;
        }

        public void OpponentShot(Point shot)
        {
        }

        public void ShotHit(Point shot, bool sunk)
        {
            var above = new Point(shot.X, shot.Y - 1);
            var below = new Point(shot.X, shot.Y + 1);
            var left = new Point(shot.X - 1, shot.Y);
            var right = new Point(shot.X + 1, shot.Y);

            Prioritize(above);
            Prioritize(below);
            Prioritize(left);
            Prioritize(right);
        }

        private void Prioritize(Point shot)
        {
            if (shot.X >= 0 &&
                shot.X < this.boardSize.Width &&
                shot.Y >= 0 &&
                shot.Y < this.boardSize.Height &&
                !this.shots.Contains(shot) &&
                !this.priority.Contains(shot))
            {
                this.priority.Enqueue(shot);
            }
        }

        public void ShotMiss(Point shot)
        {
        }

        public void GameWon()
        {
        }

        public void GameLost()
        {
        }

        public void MatchOver()
        {
        }
    }
}
