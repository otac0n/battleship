﻿namespace Battleship
{
    using System;
    using System.Collections.Generic;

    public class RandomOpponent : IBattleshipOpponent
    {
        public string Name { get { return "Random"; } }

        public Version Version { get { return this.version; } }

        Random rand = new Random();
        Version version = new Version(1, 1);
        Size gameSize;
        private int[] shipLengths;

        public void NewGame(Size size, TimeSpan timeSpan, int[] shipLengths)
        {
            this.gameSize = size;
            this.shipLengths = shipLengths;
        }

        public IList<Ship> PlaceShips()
        {
            var ships = new Ship[this.shipLengths.Length];

            for (int i = 0; i < ships.Length; i++)
            {
                ships[i] = new Ship(
                    this.shipLengths[i],
                    new Point(
                        this.rand.Next(this.gameSize.Width),
                        this.rand.Next(this.gameSize.Height)),
                    (ShipOrientation)this.rand.Next(2));
            }

            return ships;
        }

        public Point GetShot()
        {
            return new Point(
                rand.Next(this.gameSize.Width),
                rand.Next(this.gameSize.Height));
        }

        public void NewMatch(string opponent) { }

        public void OpponentShot(Point shot) { }

        public void ShotHit(Point shot, bool sunk) { }

        public void ShotMiss(Point shot) { }

        public void GameWon() { }

        public void GameLost() { }

        public void MatchOver() { }
    }
}
