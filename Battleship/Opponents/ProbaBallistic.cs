﻿namespace Battleship.Opponents
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public class ProbaBallistic : IBattleshipOpponent
    {
        private readonly Random rand = new Random();
        private Size gameSize;
        private int[] shipSizes;
        private List<Ship[]> states;
        private HashSet<Point> shots = new HashSet<Point>();
        private HashSet<Point> hits = new HashSet<Point>();
        private HashSet<Point> misses = new HashSet<Point>();
        private Queue<Point> priority = new Queue<Point>();

        public string Name
        {
            get { return "ProbaBallistic"; }
        }

        public Version Version
        {
            get { return new Version(1, 1); }
        }

        public void NewMatch(string opponent)
        {
        }

        public void NewGame(Size size, TimeSpan timeSpan, int[] shipSizes)
        {
            this.gameSize = size;
            this.shipSizes = shipSizes.OrderByDescending(s => s).ToArray();
            this.states = null;
            this.shots.Clear();
            this.misses.Clear();
            this.hits.Clear();
        }

        private void GenStates(int ship, Ship[] ships)
        {
            if (ship >= ships.Length)
            {
                if (this.states.Count >= 2000)
                {
                    this.states = null;
                    return;
                }

                var newShips = new Ship[ships.Length];
                Array.Copy(ships, newShips, ships.Length);
                this.states.Add(newShips);
            }
            else
            {
                var size = this.shipSizes[ship];

                for (int o = 0; o < 2; o++)
                {
                    var orientation = (ShipOrientation)o;

                    var width = this.gameSize.Width -
                        (orientation == ShipOrientation.Horizontal ? size - 1 : 0);

                    var height = this.gameSize.Height -
                        (orientation == ShipOrientation.Vertical ? size - 1 : 0);

                    for (int x = 0; x < width; x++)
                    {
                        for (int y = 0; y < height; y++)
                        {
                            ships[ship] = new Ship(size, new Point(x, y), orientation);

                            bool conflicts = false;
                            foreach (var miss in this.misses)
                            {
                                if (ships[ship].IsAt(miss))
                                {
                                    conflicts = true;
                                    break;
                                }
                            }

                            for (int other = 0; !conflicts && other < ship; other++)
                            {
                                if (ships[ship].ConflictsWith(ships[other]))
                                {
                                    conflicts = true;
                                }
                            }

                            if (conflicts)
                            {
                                continue;
                            }

                            GenStates(ship + 1, ships);
                            if (this.states == null)
                            {
                                return;
                            }
                        }
                    }
                }
            }
        }

        public IList<Ship> PlaceShips()
        {
            var ships = new Ship[this.shipSizes.Length];

            for (int i = 0; i < ships.Length; i++)
            {
                ships[i] = new Ship(
                    this.shipSizes[i],
                    GetRandomPoint(1),
                    (ShipOrientation)this.rand.Next(2));
            }

            return ships;
        }

        public Point GetShot()
        {
            if (this.misses.Count > 50 && this.states == null)
            {
                this.states = new List<Ship[]>();
                GenStates(0, new Ship[shipSizes.Length]);
                foreach (var hit in this.hits)
                {
                    ApplyHit(hit);
                }
            }

            Point point;

            if (this.states != null)
            {
                var probs = GetProbabilities();
                point = FindMostLikelyPoint(probs);
            }
            else
            {
                if (this.priority.Count > 0)
                {
                    point = this.priority.Dequeue();
                }
                else
                {
                    do
                    {
                        point = GetRandomPoint(2);
                    } while (this.shots.Contains(point));
                }
            }

            this.shots.Add(point);
            return point;
        }

        private void ApplyHit(Point hit)
        {
            if (this.states == null)
            {
                return;
            }

            for (int i = 0; i < this.states.Count; i++)
            {
                var remove = true;
                foreach (var ship in this.states[i])
                {
                    if (ship.IsAt(hit))
                    {
                        remove = false;
                        break;
                    }
                }

                if (remove)
                {
                    this.states.RemoveAt(i);
                    i--;
                }
            }
        }

        private void ApplyMiss(Point miss)
        {
            if (this.states == null)
            {
                return;
            }

            for (int i = 0; i < this.states.Count; i++)
            {
                var remove = false;
                foreach (var ship in this.states[i])
                {
                    if (ship.IsAt(miss))
                    {
                        remove = true;
                        break;
                    }
                }

                if (remove)
                {
                    this.states.RemoveAt(i);
                    i--;
                }
            }
        }

        private Point FindMostLikelyPoint(int[,] probs)
        {
            int max = -1;
            var point = new Point(-1, -1);

            for (int y = 0; y < this.gameSize.Height; y++)
            {
                for (int x = 0; x < this.gameSize.Width; x++)
                {
                    if (probs[x, y] > max)
                    {
                        max = probs[x, y];
                        point = new Point(x, y);
                    }
                }
            }

            return point;
        }

        private int[,] GetProbabilities()
        {
            var board = new int[this.gameSize.Width, this.gameSize.Height];
            foreach (var shot in this.shots)
            {
                board[shot.X, shot.Y] = -1;
            }

            foreach (var state in this.states)
            {
                foreach (var ship in state)
                {
                    foreach (var l in ship.GetAllLocations())
                    {
                        if (board[l.X, l.Y] != -1)
                        {
                            board[l.X, l.Y]++;
                        }
                    }
                }
            }

            RenderBoard(board);

            return board;
        }

        [Conditional("DEBUG")]
        private void RenderBoard(int[,] board)
        {
            Console.WriteLine();

            for (int y = 0; y < this.gameSize.Height; y++)
            {
                for (int x = 0; x < this.gameSize.Width; x++)
                {
                    Console.Write("{0,5} ", board[x, y]);
                }

                Console.WriteLine();
            }
        }

        private Point GetRandomPoint(int modulo)
        {
            Point p;
            do
            {
                p = new Point(
                    this.rand.Next(this.gameSize.Width),
                    this.rand.Next(this.gameSize.Height));
            } while ((p.X + p.Y) % modulo != 0);

            return p;
        }

        public void OpponentShot(Point shot)
        {
        }

        public void ShotHit(Point shot, bool sunk)
        {
            this.hits.Add(shot);

            ApplyHit(shot);

            var above = new Point(shot.X, shot.Y - 1);
            var below = new Point(shot.X, shot.Y + 1);
            var left = new Point(shot.X - 1, shot.Y);
            var right = new Point(shot.X + 1, shot.Y);

            Prioritize(above);
            Prioritize(below);
            Prioritize(left);
            Prioritize(right);
        }

        private void Prioritize(Point shot)
        {
            if (shot.X >= 0 &&
                shot.X < this.gameSize.Width &&
                shot.Y >= 0 &&
                shot.Y < this.gameSize.Height &&
                !this.shots.Contains(shot) &&
                !this.priority.Contains(shot))
            {
                this.priority.Enqueue(shot);
            }
        }

        public void ShotMiss(Point shot)
        {
            this.misses.Add(shot);
            ApplyMiss(shot);
        }

        public void GameWon()
        {
        }

        public void GameLost()
        {
        }

        public void MatchOver()
        {
        }
    }
}
