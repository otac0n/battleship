﻿namespace Battleship
{
    using System;
    using System.Collections.Generic;

    public interface IBattleshipOpponent
    {
        string Name
        {
            get;
        }

        Version Version
        {
            get;
        }

        void NewMatch(string opponent);

        void NewGame(Size size, TimeSpan timeSpan, int[] shipSizes);

        IList<Ship> PlaceShips();

        Point GetShot();

        void OpponentShot(Point shot);

        void ShotHit(Point shot, bool sunk);

        void ShotMiss(Point shot);

        void GameWon();

        void GameLost();

        void MatchOver();
    }
}
