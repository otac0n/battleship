﻿namespace Battleship
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public sealed class Ship
    {
        private readonly Point location;
        private readonly ShipOrientation orientation;
        private readonly int length;

        public Ship(int length, Point location, ShipOrientation orientation)
        {
            if (length <= 1)
            {
                throw new ArgumentOutOfRangeException("length");
            }

            this.length = length;
            this.location = location;
            this.orientation = orientation;
        }

        public Point Location
        {
            get
            {
                return this.location;
            }
        }

        public ShipOrientation Orientation
        {
            get
            {
                return this.orientation;
            }
        }

        public int Length
        {
            get
            {
                return this.length;
            }
        }

        public bool IsValid(Size boardSize)
        {
            if (this.location.X < 0 || this.location.Y < 0)
            {
                return false;
            }

            if (this.orientation == ShipOrientation.Horizontal)
            {
                if (this.location.Y >= boardSize.Height || this.location.X + this.length > boardSize.Width)
                {
                    return false;
                }
            }
            else
            {
                if (this.location.X >= boardSize.Width || this.location.Y + this.length > boardSize.Height)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsAt(Point location)
        {
            if (this.Orientation == ShipOrientation.Horizontal)
            {
                return (this.location.Y == location.Y) && (this.location.X <= location.X) && (this.location.X + this.length > location.X);
            }
            else
            {
                return (this.location.X == location.X) && (this.location.Y <= location.Y) && (this.location.Y + this.length > location.Y);
            }
        }

        public IEnumerable<Point> GetAllLocations()
        {
            if (this.Orientation == ShipOrientation.Horizontal)
            {
                for (int i = 0; i < this.length; i++)
                {
                    yield return new Point(this.location.X + i, this.location.Y);
                }
            }
            else
            {
                for (int i = 0; i < this.length; i++)
                {
                    yield return new Point(this.location.X, this.location.Y + i);
                }
            }
        }

        public bool ConflictsWith(Ship other)
        {
            if (this.orientation == ShipOrientation.Horizontal &&
                other.orientation == ShipOrientation.Horizontal)
            {
                if (this.location.Y != other.location.Y)
                {
                    return false;
                }

                return (other.location.X < (this.location.X + this.length)) &&
                       (this.location.X < (other.location.X + other.length));
            }
            else if (this.orientation == ShipOrientation.Vertical &&
                other.orientation == ShipOrientation.Vertical)
            {
                if (this.location.X != other.location.X)
                {
                    return false;
                }

                return (other.location.Y < (this.location.Y + this.length)) &&
                       (this.location.Y < (other.location.Y + other.length));
            }
            else
            {
                Ship h, v;
                if (this.orientation == ShipOrientation.Horizontal)
                {
                    h = this;
                    v = other;
                }
                else
                {
                    h = other;
                    v = this;
                }

                return (h.location.Y >= v.location.Y) &&
                       (h.location.Y < (v.location.Y + v.length)) &&
                       (v.location.X >= h.location.X) &&
                       (v.location.X < (h.location.X + h.length));
            }
        }

        public bool IsSunk(IEnumerable<Point> shots)
        {
            foreach (Point location in this.GetAllLocations())
            {
                if (!shots.Where(s => s.X == location.X && s.Y == location.Y).Any())
                {
                    return false;
                }
            }

            return true;
        }
    }
}
