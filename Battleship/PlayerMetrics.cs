﻿namespace Battleship
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PlayerMetrics
    {
        private readonly IBattleshipOpponent op;
        private readonly Size boardSize;
        private readonly List<int> shipSizes;

        public PlayerMetrics(IBattleshipOpponent op, Size boardSize, params int[] shipSizes)
        {
            if (op == null)
            {
                throw new ArgumentNullException("op1");
            }

            if (boardSize.Width <= 2 || boardSize.Height <= 2)
            {
                throw new ArgumentOutOfRangeException("boardSize");
            }

            if (shipSizes == null || shipSizes.Length < 1)
            {
                throw new ArgumentNullException("shipSizes");
            }

            if (shipSizes.Where(s => s <= 0).Any())
            {
                throw new ArgumentOutOfRangeException("shipSizes");
            }

            if (shipSizes.Sum() >= (boardSize.Width * boardSize.Height))
            {
                throw new ArgumentOutOfRangeException("shipSizes");
            }

            this.op = op;
            this.boardSize = boardSize;
            this.shipSizes = new List<int>(shipSizes);
        }

        public long[] CalculateMetrics()
        {
            var hits = new long[this.boardSize.Width * this.boardSize.Height];

            this.op.NewMatch("Metrics");

            for (int i = 0; i < 10000; i++)
            {
                this.op.NewGame(this.boardSize, TimeSpan.MaxValue, this.shipSizes.ToArray());
                this.op.PlaceShips();

                var ships = this.CreateShips();

                var shots = new HashSet<Point>();

                while (true)
                {
                    Point shot;
                    do
                    {
                        shot = this.op.GetShot();
                    } while (shots.Contains(shot));
                    shots.Add(shot);

                    var hit = ships.SingleOrDefault(s => s.IsAt(shot));
                    if (hit != null)
                    {
                        if (hit.IsSunk(shots))
                        {
                            this.op.ShotHit(shot, true);
                            ships.Remove(hit);
                        }
                        else
                        {
                            this.op.ShotHit(shot, false);
                        }
                    }
                    else
                    {
                        this.op.ShotMiss(shot);
                    }

                    if (ships.Count == 0)
                    {
                        break;
                    }
                }

                this.op.GameWon();

                hits[shots.Count - 1]++;
            }

            return hits;
        }

        private List<Ship> CreateShips()
        {
            var rand = new RandomOpponent();

            rand.NewMatch("None");
            rand.NewGame(this.boardSize, TimeSpan.Zero, this.shipSizes.ToArray());

            List<Ship> ships;
            do
            {
                ships = Validate(rand.PlaceShips());
            } while (ships == null);

            return ships;
        }

        private List<Ship> Validate(IList<Ship> ships)
        {
            if (ships == null)
            {
                return null;
            }

            var copy = ships.ToList();

            if (copy.Any(s => s == null || !s.IsValid(this.boardSize)))
            {
                return null;
            }

            var lengths = this.shipSizes.ToList();
            foreach (var s in copy)
            {
                if (!lengths.Remove(s.Length))
                {
                    return null;
                }
            }

            if (lengths.Any())
            {
                return null;
            }

            for (int i = 0; i < copy.Count; i++)
            {
                for (int j = i + 1; j < copy.Count; j++)
                {
                    if (copy[i].ConflictsWith(copy[j]))
                    {
                        return null;
                    }
                }
            }

            return copy;
        }
    }
}
