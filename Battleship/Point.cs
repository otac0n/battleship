﻿namespace Battleship
{
    public struct Point
    {
        private readonly int x;
        private readonly int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X
        {
            get { return this.x; }
        }

        public int Y
        {
            get { return this.y; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point))
            {
                return false;
            }

            Point point = (Point)obj;
            return (point.x == this.x) &&
                   (point.y == this.y);
        }

        public override int GetHashCode()
        {
            return this.x ^ this.y;
        }

        public static bool operator ==(Point left, Point right)
        {
            return (left.x == right.x) &&
                   (left.y == right.y);
        }

        public static bool operator !=(Point left, Point right)
        {
            return (left.x != right.x) ||
                   (left.y != right.y);
        }

        public Point Add(int x, int y)
        {
            return new Point(this.x + x, this.y + y);
        }

        public Point Add(Size d)
        {
            return new Point(this.x + d.Width, this.y + d.Height);
        }
    }
}
